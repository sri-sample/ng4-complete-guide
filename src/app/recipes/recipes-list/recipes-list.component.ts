import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
   recipes : Recipe[] = [
     new Recipe('A Test Recipe','Checking descriptios of test recipe',
     'https://addapinch.com/wp-content/uploads/2010/12/beef-stroganoff-recipe-3019-2.jpg')
   ];
  constructor() { }

  ngOnInit(): void {
  }

}
